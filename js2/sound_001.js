let micro;
let amplitude;
let font;
let sensitivity = 50;

function preload() {
    font = loadFont('../font/SpaceMono-Regular.ttf');
}

function setup() {
    let cnv = createCanvas(640, 480);
    cnv.parent('theCanvas');
  fill(255);
  textFont(font);
  textAlign(CENTER);
  // on récupère l'audio - micro
  micro = new p5.AudioIn();
  micro.start();
}

function draw() {
  background(0);
  let level = micro.getLevel() * sensitivity;
  let fontSize = map(level, 0, 1, 10, 200);
  textSize(fontSize);
  text("SOUND", width/2, height/2);


}
    