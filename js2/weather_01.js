/**
 *  API Call with :
 * https://openweathermap.org/current
 * NOTE : Create an account and get your own API Key ;–)
 */
let position;
let data;
let font;
let weather;
let city, temp, windSpeed, windOrientation;
let lieu, degres, img;
    
function preload() {
  let apiKey = '20c14d4348289908bac9097c1dee87cd'
  let city = 'Amiens';
  let url = 'https://api.openweathermap.org/data/2.5/weather?q='+city+'&units=metric'
  let apiCall = url + '&appid=' + apiKey;
  data = loadJSON(apiCall);
  console.log(data); // this displays all available data in the web navigator console
  font = loadFont('../font/SpaceMono-Regular.ttf');    
}

function setup() {
  let cnv = createCanvas(640, 480);
  cnv.parent('theCanvas');

  textFont(font);
  weather = data.weather[0].description;
  city = data.name;
  temp = data.main.temp;
  windSpeed = data.wind.speed;
  windOrientation = data.wind.deg;
}

function draw() {
  background(0);
  noStroke();
  textSize(35);
  text(city,50,40);
  textSize(28);

  text('Conditions: '+weather,50,100);
  let tempColour = map(temp, -10, 40, 150, 255);
  fill(tempColour,30,tempColour);
  text('Temp: '+temp,50,130);

  fill(255);
  text('Wind Speed: '+windSpeed,50,160);
  text('Orientation: '+windOrientation,50,190);

  // voir la fonction ci-dessous 
  windGraphic(windSpeed, windOrientation);

}

/**
 * A grid of lines to represent the wind
 * @param {determines mouvement} speed 
 * @param {determines angle} orientation 
 */
function windGraphic(speed, orientation) {
  // Wind animation variables
  let noiseScale = 0.02;
  let noiseAmm = 50.0;
  let lineLnH = 10.0; // length of lines
  stroke(255);
  for (let yPos=75; yPos<height-50; yPos+=20) {
    for (let xPos=75; xPos<width-75; xPos+=20) {
      let ws =  map(speed, 0, 150, 0, 0.5);
      let z = frameCount*ws;
      let offSet = noise(xPos*noiseScale, yPos*noiseScale, z) * noiseAmm;
      push();
      translate(xPos, yPos);
      rotate(radians(orientation));
      line(-lineLnH+offSet, -lineLnH, lineLnH, lineLnH);
      pop();
    }
  }
}