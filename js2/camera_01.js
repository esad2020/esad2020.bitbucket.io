/**
 * Basic camera setup
 */


let video;
let taille = 20;

function setup() {
let cnv = createCanvas(640, 480);
  cnv.parent('theCanvas');
  pixelDensity(1);
  video = createCapture(VIDEO);
  video.size(width/taille, height/taille);
  video.hide();
}

function draw() {
  background(255);
  image(video, 0, 0, 640, 480);
  
  if(mouseIsPressed){
    filter(GRAY);
  }
}

/*
Try these filters :
filter(POSTERIZE, 5); 
filter(THRESHOLD, 0.35);
filter(GRAY);
filter(BLUR, 3);

}*/

