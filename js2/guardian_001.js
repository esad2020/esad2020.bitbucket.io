/**
 *  API Call with :
 * https://open-platform.theguardian.com
 * NOTE : Create an account and get your own API Key ;–)
 */


let font;
let articles = []; // empty list variable
let searchWord = 'Virus';

function preload() {
    const apiKey = '86e08f60-6685-4b42-a402-12de20cc407d';
    const apiCall = "https://content.guardianapis.com/search?q=" + searchWord + "&api-key=" + apiKey;
    const data = loadJSON(apiCall, getData, 'jsonp');
    console.log(data); // this displays all available data in the web navigator console
    font = loadFont('../font/SpaceMono-Regular.ttf');

}



function setup() {
    let cnv = createCanvas(640, 480);
    cnv.parent('theCanvas');
    textFont(font);
    fill(255);
}

function draw() {
    background(0);
    noStroke();
    textSize(28);
    text('Todays Guardian Articles', 15, 50);
    textSize(22);
    text('Search term: '+searchWord, 15, 80);
    textSize(12);
    let y = 175;
    for (let i = 0; i < articles.length; i++) {
        text(articles[i], 15, y);
        y += 18;
    }
}

/**
 * Parses data for articles and adds them to our article variable list
 * @param {data from API} theData 
 */
async function getData(theData) {
    const dataResponse = theData.response.results;
    //console.log(dataResponse);
    for (let i = 0; i < dataResponse.length; i++) {
        const theArticles = dataResponse[i].webTitle;
        articles.push(theArticles);
    }
}