/**
 * Source : 
 * https://kylemcdonald.github.io/cv-examples/
 *
 */


let capture;
let previousPixels;
let w = 640;
let h = 480;

function setup() {
    capture = createCapture({
        audio: false,
        video: {
            width: w,
            height: h
        }
    }, function() {
        console.log('capture ready.')
    });
    capture.elt.setAttribute('playsinline', '');
    capture.size(w, h);
    let cnv = createCanvas(w, h);
    cnv.parent('theCanvas');
    capture.hide();
}

function copyImage(src, dst) {
    dst = createImage(src.width, src.height);
    let n = src.length;
    if (!dst || dst.length != n) dst = new src.constructor(n);
    while (n--) dst[n] = src[n];
    return dst;
}

function draw() {
    capture.loadPixels();
    let total = 0;
    if (capture.pixels.length > 0) { 
        if (!previousPixels) {
            previousPixels = copyImage(capture.pixels, previousPixels);
        } else {
            let w = capture.width,
                h = capture.height;
                let i = 0;
                let pixels = capture.pixels;
                let thresholdAmount = select('#thresholdAmount').value() * 255. / 100.;
            thresholdAmount *= 3; // 3 for r, g, b
            for (let y = 0; y < h; y++) {
                for (let x = 0; x < w; x++) {
                    // calculate the differences
                    let rdiff = Math.abs(pixels[i + 0] - previousPixels[i + 0]);
                    let gdiff = Math.abs(pixels[i + 1] - previousPixels[i + 1]);
                    let bdiff = Math.abs(pixels[i + 2] - previousPixels[i + 2]);
                    // copy the current pixels to previousPixels
                    previousPixels[i + 0] = pixels[i + 0];
                    previousPixels[i + 1] = pixels[i + 1];
                    previousPixels[i + 2] = pixels[i + 2];
                    let diffs = rdiff + gdiff + bdiff;
                    let output = 0;
                    if (diffs > thresholdAmount) {
                        output = 255;
                        total += diffs;
                    }
                    pixels[i++] = output;
                    pixels[i++] = output;
                    pixels[i++] = output;
                    // also try this:
                     //pixels[i++] = rdiff;
                     //pixels[i++] = gdiff;
                     //pixels[i++] = bdiff;
                    i++; // skip alpha
                }
            }
        }
    }
    // need this because sometimes the frames are repeated
    if (total > 0) {
        select('#motion').elt.innerText = total;
        capture.updatePixels();
        image(capture, 0, 0, 640, 480);
    }

    //background(0);
    /////////////////////////////////////////////////////// Voici où l'on récupère la valeur du mouvement...
    // change ellipse size depending on motion input
    let dia = map(total, 0, 15026260, 0, 350);
    noStroke();
    fill(0,55,235);
    ellipse(100,100, dia, dia);
}
