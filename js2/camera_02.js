let video;
let taille = 20;
    
function setup() {
    let cnv = createCanvas(640, 480);
    cnv.parent('theCanvas');
    pixelDensity(1);
    video = createCapture(VIDEO);
    video.size(width/taille, height/taille);
    video.hide();
}

function draw() {
  background(255);

  video.loadPixels();
  loadPixels();
  for (let y = 0; y < video.height; y++) {
    for (let x = 0; x < video.width; x++) {
        let index = (x+y * video.width)*4;
        let r = video.pixels[index+0];
        let g = video.pixels[index+1];
        let b = video.pixels[index+2];

        let bright = (r+g+b)/3; // calculates pixel brightness

        // Mapping these values for 3 different variables:
        let w = map(bright, 0, 255, taille*1.15, 0);
        let colVal = map(bright, 0, 255, 0, 255);
        let angle = map(bright, 0, 255, 0, 230);

      noStroke();
      if(mouseIsPressed){
        fill(r, g, b, colVal);
      }else {
        fill(colVal);
      }
      rectMode(CENTER);
      push();
      translate(x*taille, y*taille);
      rotate(radians(angle));
      rect(0,0, w, w);
      pop();
    }
  } 
}
    