/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: recursion_02
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/
let f;
let pg;
let txt = "type";

// tu peux changer la fonte d'origine et qui aura un effet sur la forme
let dio = "Diodon-v_1";
let plex = "IBMPlexSans-Bold";

let fontSize;
let croixLen;
let sw;
let angle;
let xStep, yStep;
let isCross = false;

function preload() {
  f = loadFont('../font/IBMPlexSans-Bold.ttf');
}
function setup() {
  var c = createCanvas(700, 560);
  c.parent('theCanvas');
  background(0);
  smooth();
  //textFont(f);
  //textSize(fontSize);
  pg = createGraphics(width, height);
  pg.colorMode(HSB,360,100,100);
  pg.textFont(f, fontSize);
  pg.textSize( fontSize );
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
  background(0);
  updatePGraphics(); 
  fontSize = slider1.value;
  croixLen = slider2.value;
  sw = slider3.value;
  angle = slider4.value;
  xStep = parseInt(slider5.value);
  yStep = parseInt(slider6.value);

  pg.loadPixels();
  loadPixels();
 
 const d = pixelDensity();

  for (let y=0; y<height; y+=yStep) {
    for (let x=0; x<width; x+=xStep) {
      const i = 4 * d*(y * d*width + x);
      const [r, g, b] = [pg.pixels[i], pg.pixels[i + 1], pg.pixels[i + 2]];
        if(r <= 80 && b <= 80 && g <= 80) {
        stroke(255);
        strokeWeight(sw);
        if (isCross) {
          push();
          translate(x, y);
          croix(0, 0, croixLen); // la forme >>> une fonction ci-dessous
          pop();
          
        } else {
          push();
          translate(x, y);
          rotate(radians(angle));
          line(-croixLen, -croixLen, croixLen, croixLen);
          pop();
           
       }
         
      }

    }
  }
}


function croix( x,  y,  len) {
  rectMode(CENTER);
  push();
  translate(x, y);
  rotate(radians(angle));
  line(-len, -len, len, len);
  pop();
  push();
  translate(x, y);
  rotate(radians(-angle));
  line(-len, len, len, -len);
  pop();
}


function updatePGraphics() {
  //g.beginDraw();
  pg.colorMode(HSB,360,100,100);
  pg.background(255);
  pg.textFont(f, fontSize);
  pg.textSize( fontSize );
  pg.textAlign(CENTER, CENTER);
  pg.fill(0);
  pg.text(txt, pg.width/2, pg.height/2.9);
  //g.endDraw();

  // DEBUG
  //image(pg, 0, 0);
}

//////////////////// INTERACTION : KEYS //////////////////////////
function keyPressed() {

  if (keyCode == BACKSPACE) {
    if (txt.length > 0) {
      txt = txt.substring(0, txt.length-1);
    }
  } else if (keyCode == DELETE) {
    txt = "";
  } else if (keyCode != ENTER && key != '-' && keyCode != ALT && keyCode != RIGHT && keyCode != LEFT && keyCode != CONTROL && keyCode != SHIFT) {
    fill(255);
    txt = txt + key;
  }
}

// Slider update:
function outputUpdateSlider1(v) {
  document.querySelector('#fontSize').value = v;
}
function outputUpdateSlider2(v) {
  document.querySelector('#len').value = v;
}

function outputUpdateSlider3(v) {
  document.querySelector('#sw').value = v;
}
function outputUpdateSlider4(v) {
  document.querySelector('#angle').value = v;
}
function outputUpdateSlider5(v) {
  document.querySelector('#xStep').value = v;
}
function outputUpdateSlider6(v) {
  document.querySelector('#yStep').value = v;
}

function outputUpdateSlider7(v) {
  document.querySelector('#button').value = v;
}

function msg() {
  isCross =!isCross;
  button.value=isCross;
}
