/**
 * Sketch_01
 * Animer un cercle
 */

let x = 0; // une variable

function setup(){
    let cnv = createCanvas(640, 480);
    cnv.parent('theCanvas');
    background(0);
  }
 
  function draw(){
    background(0);
    noStroke();
    fill(255,0,0);
    ellipse(x, 200, 225, 225);
    x+=3;
    if(x>=width+225){
        x = -125;
    }
 
  }
 