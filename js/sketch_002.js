/**
 * Sketch_02
 * Dessiner un cercle
 */


function setup(){
    let cnv = createCanvas(640, 480);
    cnv.parent('theCanvas');
    background(0);
    noStroke();
    fill(255);
    text("Click & drag on canvas", 25, height-25);
 
  }
 
  function draw(){
    if(mouseIsPressed){
        let dia = sin(frameCount * 0.015) * 35;
        fill(55,25,255);
        ellipse(mouseX, mouseY, dia, dia);
    } 
  }
 