/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: 2 Sketches or more on 1 page
* Author: m.Webster 2019
* https://area03.bitbucket.io
*
*/

// On englobe chaque sketch dans une variable
// ici notre variable s'appelle skech1
// on fait appel à cette variable dans le HTML pour l'afficher ;–)
// IMPORTANT : Note bien que tu dois (malheureusement) rajouter le mot clé
// 'sketch.' avant chaque fonction (pas les variables)
let sketch1 = function(sketch) {
   let dia = 150;

   /////////////////////////// SETUP ////////////////////////////
   sketch.setup = function() {
       // note bien le mot clé sketch. qui précède chaque fonction
     sketch.createCanvas(490, 380);
     sketch.background(0);
     sketch.smooth();

   }
   
   /////////////////////////// DRAW ////////////////////////////
   sketch.draw = function() {
     sketch.background(0);
     sketch.noStroke();
     sketch.fill(255,255,0);
    sketch.ellipse(sketch.frameCount*1.5%sketch.width, sketch.height/2, dia, dia); 
         
       
   }
      
   sketch.keyTyped = function() {
     // Increase the size of our cirlces
     if (sketch.key === '+') {
       dia += 1;
     } 
     // Decrease the size of our cirlces
     if (sketch.key === '-') {
       dia -= 1;
     }

   }
   
   };
   
   let myp5 = new p5(sketch1, 'sketch1');
   
    
   ///////////////////////////////////////////////////////////////////// Sketch 2 >>>
   let sketch2 = function( sketch) {
    let dia = 150;

    /////////////////////////// SETUP ////////////////////////////
    sketch.setup = function() {
      sketch.createCanvas(490, 380);
      sketch.background(0);
      sketch.smooth();
 
    }
    
    /////////////////////////// DRAW ////////////////////////////
    sketch.draw = function() {
      sketch.background(0);
      sketch.noStroke();
      sketch.fill(0,0,255);
     sketch.rect(sketch.frameCount*2.25%sketch.width, sketch.height/2, dia, dia); 
          
        
    }
       
    sketch.keyTyped = function() {
      // Increase the size of our cirlces
      if (sketch.key === '+') {
        dia += 1;
      } 
      // Decrease the size of our cirlces
      if (sketch.key === '-') {
        dia -= 1;
      }
 
    }
   };
   
   let myp52 = new p5(sketch2, 'sketch2');
   