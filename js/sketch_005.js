let font;
let letters = [];
    
function preload() {
    font = loadFont("../font/SpaceMono-Regular.ttf");
}
    
function setup() {
    let cnv = createCanvas(640, 480);
    cnv.parent('theCanvas');
    background(0);
    textFont(font);
}
    
function draw() {
    background(0); 
    for (let i =0; i<letters.length; i++){        
        letters[i].update();
        } 
        
        textSize(25);
        fill(255);
        text("Start typing..." , 25, height-30);        
}
    
    
function keyPressed() {
    letters.push(new Letter());
}
    
class Letter{
    constructor() {
        this.x= int(random(width));
        this.y= int(random(height));
        this.alpha = 255;
        this.text = `${key}`;
        this.textSize = random(15, 80);
    }
    
    update() {
        fill(255, this.alpha);
        textSize(this.textSize);
        text(this.text , this.x , this.y);
        this.y +=0.5;
        this.alpha -= 0.5;        
    } 
}