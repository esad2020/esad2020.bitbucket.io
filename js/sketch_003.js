
let num = 50;
let displayFont;
let x, y;

function preload(){
  displayFont = loadFont("../font/SpaceMono-Regular.ttf");
}

function setup() {
let cnv = createCanvas(640, 480);
cnv.parent('theCanvas');
background(0);
textFont(displayFont);
textSize(16);
x = width/2;
y = height/2;

 fill(200);
 text("Click mouse to restart", 10, height-12);
}

function draw() {
  for( var i=0; i<num; i++){
      noStroke();
      fill(255,33);
      x += random(-2,2);
      y  += random(-2,2);
      this.dia = sin(frameCount*0.015) * 9;
      ellipse(x, y, dia, dia);
  }

}

function mousePressed(){
  setup();
}

