/*
TEXT IN CANVAS
*/

function setup() {
    let cnv = createCanvas(640, 480);
    cnv.parent('theCanvas');
    background(0);
    // voici un texte crée comme un élément HTML
    let mots = 'Ce texte est un élément HTML crée en P5js !'
    let text = createDiv(mots);
    text.style("font-family", "SpaceMono-Regular");
    text.style("font-size", "27pt");
    // qui se positionne avec cette fonction
    text.position(200, windowHeight-100);
  } 
  
  function draw() {

    let motion= sin(frameCount*0.017)*50;
    noStroke();
     fill(0,0,255);
     ellipse(frameCount%width, 350+motion, motion, motion);
  
    // font name refers to font specified in css
    textFont('SpaceMono-Bold');
    textSize(33);
  
    let canvasText1 = 'Ceci est du texte crée en P5js';
    let canvasText2 = 'mais qui fait partie du canvas'
  
    fill(0);
    text(canvasText1, 10, height/2 + 100);
    text(canvasText2, 10, height/2 + 130);
    
    fill(255,0,0);
    ellipse(width/2+motion, frameCount%height, motion, motion);
     
    fill(255,255,0);
     ellipse(mouseX, mouseY, 50, 50);
  
  }
  
  