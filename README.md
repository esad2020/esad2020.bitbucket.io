# [ESAD_2020](https://esad2020.bitbucket.io)


[![PyPI](https://img.shields.io/pypi/l/fsfe-reuse.svg)](https://www.gnu.org/licenses/gpl-3.0.html)
[![reuse compliant](https://img.shields.io/badge/reuse-compliant-green.svg)](https://git.fsfe.org/reuse/reuse)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
---


## Introduction

A small collection of sketches to introduce P5js.

## Contents
A collection of simple P5js sketches.

* DP_P5js - JavaScript(P5js) sketches
* ...

## Website

This repository is accompanied by a website : [esad2020](https://esad2020.bitbucket.io)

## Install

The programs in this repository can be compiled with P5js in a browser.

[https://p5js.org/](https://p5js.org/)

## Contact & Sundries

* mark.webster[at]wanadoo.fr
* Version v0.05
* Tools used : P5js

## Contribute
To contribute to this project, please fork the repository and make your contribution to the
fork, then open a pull request to initiate a discussion around the contribution. You can equally contact me directly via email. 

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

For more information https://www.gnu.org/licenses/gpl-3.0.en.html

The program in this repository meet the requirements to be REUSE compliant,
meaning its license and copyright is expressed in such as way so that it
can be read by both humans and computers alike.

For more information, see https://reuse.software/
